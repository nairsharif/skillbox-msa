package com.yamangulov.repo.repository;

import com.yamangulov.repo.entity.City;
import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Integer> {
}
