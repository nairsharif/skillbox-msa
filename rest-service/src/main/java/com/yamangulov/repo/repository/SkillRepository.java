package com.yamangulov.repo.repository;

import com.yamangulov.repo.entity.Skill;
import org.springframework.data.repository.CrudRepository;

public interface SkillRepository extends CrudRepository<Skill, Integer> {
}
