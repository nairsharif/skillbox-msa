--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Debian 14.4-1.pgdg110+1)
-- Dumped by pg_dump version 14.4 (Ubuntu 14.4-1.pgdg21.10+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: msa
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO msa;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.city (
    id smallint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.city OWNER TO postgres;

--
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.city ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact (
    id uuid DEFAULT gen_random_uuid(),
    email character varying NOT NULL,
    phone character varying NOT NULL,
    deleted boolean DEFAULT false
);


ALTER TABLE public.contact OWNER TO postgres;

--
-- Name: gender; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gender (
    id smallint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.gender OWNER TO postgres;

--
-- Name: gender_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.gender ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.gender_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: hard_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hard_skills (
    id uuid DEFAULT gen_random_uuid(),
    user_id uuid NOT NULL,
    skill_id smallint NOT NULL
);


ALTER TABLE public.hard_skills OWNER TO postgres;

--
-- Name: profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profile (
    id uuid DEFAULT gen_random_uuid(),
    name character varying NOT NULL,
    surname character varying NOT NULL,
    second_name character varying,
    birth_date date NOT NULL,
    avatar_link character varying,
    information text,
    city_id smallint,
    contact_id uuid NOT NULL,
    gender_id smallint,
    deleted boolean DEFAULT false
);


ALTER TABLE public.profile OWNER TO postgres;

--
-- Name: skill; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skill (
    id smallint NOT NULL,
    name character varying
);


ALTER TABLE public.skill OWNER TO postgres;

--
-- Name: skill_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.skill ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.skill_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: subscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subscription (
    id uuid DEFAULT gen_random_uuid(),
    subscriber_user_id uuid NOT NULL,
    subscribed_user_id uuid NOT NULL
);


ALTER TABLE public.subscription OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id uuid DEFAULT gen_random_uuid(),
    profile_id uuid NOT NULL,
    deleted boolean DEFAULT false
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Data for Name: city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.city (id, name) FROM stdin;
\.


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contact (id, email, phone, deleted) FROM stdin;
8fd7f4b5-ad0d-489f-a064-e7752c399a78	test@test.ru	9106664893	f
e0d8e775-ec04-4230-9103-b85fd1e214d8	test222@test.ru	999999999	f
\.


--
-- Data for Name: gender; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gender (id, name) FROM stdin;
\.


--
-- Data for Name: hard_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hard_skills (id, user_id, skill_id) FROM stdin;
\.


--
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profile (id, name, surname, second_name, birth_date, avatar_link, information, city_id, contact_id, gender_id, deleted) FROM stdin;
90dbc46d-53c2-421e-b7ce-241a2dbd86b7	Petr	Petrov	\N	2000-01-21	\N	\N	\N	8fd7f4b5-ad0d-489f-a064-e7752c399a78	\N	f
e0e3984d-6449-4034-a0b8-20e6df9c748f	Ivan	Ivanov	\N	2000-11-24	\N	\N	\N	e0d8e775-ec04-4230-9103-b85fd1e214d8	\N	f
\.


--
-- Data for Name: skill; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.skill (id, name) FROM stdin;
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subscription (id, subscriber_user_id, subscribed_user_id) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, profile_id, deleted) FROM stdin;
ae666352-da3a-4c90-ad70-6a8f500fcbcc	e0e3984d-6449-4034-a0b8-20e6df9c748f	f
aa1c1b72-5bfc-4da2-ab77-102226c71dbc	90dbc46d-53c2-421e-b7ce-241a2dbd86b7	f
\.


--
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.city_id_seq', 1, false);


--
-- Name: gender_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gender_id_seq', 1, false);


--
-- Name: skill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.skill_id_seq', 1, false);


--
-- Name: city city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- Name: contact contact_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT contact_id_key UNIQUE (id);


--
-- Name: gender gender_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gender
    ADD CONSTRAINT gender_name_key UNIQUE (name);


--
-- Name: gender gender_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gender
    ADD CONSTRAINT gender_pkey PRIMARY KEY (id);


--
-- Name: hard_skills hard_skills_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hard_skills
    ADD CONSTRAINT hard_skills_id_key UNIQUE (id);


--
-- Name: profile profile_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_id_key UNIQUE (id);


--
-- Name: skill skill_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT skill_pkey PRIMARY KEY (id);


--
-- Name: subscription subscription_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_id_key UNIQUE (id);


--
-- Name: user user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_id_key UNIQUE (id);


--
-- Name: city_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX city_id_idx ON public.profile USING hash (city_id);


--
-- Name: gender_id_city_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gender_id_city_id_idx ON public.profile USING btree (gender_id, city_id);


--
-- Name: gender_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gender_id_idx ON public.profile USING hash (gender_id);


--
-- Name: hard_skills hard_skills_skill_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hard_skills
    ADD CONSTRAINT hard_skills_skill_fk FOREIGN KEY (skill_id) REFERENCES public.skill(id);


--
-- Name: hard_skills hard_skills_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hard_skills
    ADD CONSTRAINT hard_skills_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: profile profile_city_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_city_fk FOREIGN KEY (city_id) REFERENCES public.city(id);


--
-- Name: profile profile_contact_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_contact_fk FOREIGN KEY (contact_id) REFERENCES public.contact(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: profile profile_gender_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_gender_fk FOREIGN KEY (gender_id) REFERENCES public.gender(id);


--
-- Name: subscription subscription_subscribed_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_subscribed_user_fk FOREIGN KEY (subscribed_user_id) REFERENCES public."user"(id);


--
-- Name: subscription subscription_subscriber_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_subscriber_user_fk FOREIGN KEY (subscriber_user_id) REFERENCES public."user"(id);


--
-- Name: user user_profile_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_profile_fk FOREIGN KEY (profile_id) REFERENCES public.profile(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: TABLE city; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.city TO msa;


--
-- Name: TABLE contact; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.contact TO msa;


--
-- Name: TABLE gender; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.gender TO msa;


--
-- Name: TABLE hard_skills; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.hard_skills TO msa;


--
-- Name: TABLE profile; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.profile TO msa;


--
-- Name: TABLE skill; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.skill TO msa;


--
-- Name: TABLE subscription; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.subscription TO msa;


--
-- Name: TABLE "user"; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public."user" TO msa;


--
-- PostgreSQL database dump complete
--

